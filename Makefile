.PHONY: npm
npm:
	wasm-pack build --target bundler

.PHONY: wordlist
wordlist:
	./make_wordlist.sh > src/generators/wordlist.rs
