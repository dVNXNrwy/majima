use wasm_bindgen::prelude::*;
pub mod generators;

#[wasm_bindgen]
pub fn version() -> String {
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");
    return VERSION.to_string()
}
