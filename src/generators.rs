use rand::distributions::{Alphanumeric, DistString};
use rand::Rng;
use wasm_bindgen::prelude::*;
mod wordlist;

#[wasm_bindgen]
pub fn random(length: usize) -> String {
    Alphanumeric.sample_string(&mut rand::thread_rng(), length)
}

#[wasm_bindgen]
pub fn memorable(length: usize, casing: Option<String>) -> String {
    if length == 0 {
        return "".to_string()
    }
    let mut username_vec: Vec<&str> = Vec::new();
    for _ in 0..length {
        username_vec.push(wordlist::WORDS[rand::thread_rng().gen_range(0..wordlist::WORDS.len())]);
    }
    match casing.unwrap_or_default().as_str() {
        "pascal"=> {
            let pascal: Vec<String> = username_vec.iter().map(|x| to_uppercase(x)).collect();
            return pascal.join("")
        },
        "camel"=> {
            let mut camel: Vec<String> = vec![username_vec[0].to_string()];
            camel.extend(username_vec[1..].iter().map(|x| to_uppercase(x)));
            return camel.join("")
        },
        "snake" => return username_vec.join("_"),
        "kebab" | _ => return username_vec.join("-"),
    }
}

fn to_uppercase(s: &str) -> String {
    s[0..1].to_uppercase() + &s[1..]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_random_length() {
        let length = 10;
        assert_eq!(random(length).len(), length);
    }

    #[test]
    fn test_memorable_length() {
        let length = 10;
        assert!(memorable(length, Some("kebab".to_string())).len() > length*2);
        assert!(memorable(length, Some("kebab".to_string())).matches('-').count() >= length-1);
    }
}
