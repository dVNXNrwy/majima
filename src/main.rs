use clap::Parser;
mod generators;

/// Simple program for generating usernames
#[derive(Parser, Debug)]
#[command(author = "gumball-overall", version, about, long_about = None)]
struct Args {
    /// Generate a memorable username using words
    #[arg(short)]
    memorable: bool,

    /// Length of username (number of words if memorable)
    #[arg(short, long, default_value_t)]
    length: usize,

    /// Casing to use when generating a memorable username (pascal, camel, snake, kebab)
    #[arg(short, long, default_value_t)]
    casing: String,
}

fn main() {
    let args = Args::parse();
    let mut length = args.length;
    let username;
    if args.memorable || !args.casing.is_empty() {
        if length == 0 {
            length = 3;
        }
        username = generators::memorable(length, Some(args.casing))
    } else {
        if length == 0 {
            length = 20;
        }
        username = generators::random(length)
    }
    println!("{}", username)
}
