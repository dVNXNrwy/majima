# Archived

This project has moved to https://git.sr.ht/~cucumber-zoom/majima

# majima

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/gumball-overall/majima?branch=main)](https://gitlab.com/gumball-overall/majima/-/pipelines) [![Crates.io](https://img.shields.io/crates/v/majima)](https://crates.io/crates/majima) [![npm](https://img.shields.io/npm/v/majima)](https://www.npmjs.com/package/majima) [![Built with Nix](https://img.shields.io/static/v1?logo=nixos&logoColor=white&label=&message=Built%20with%20Nix&color=41439a)](https://builtwithnix.org/)

Generate random usernames quickly and in various formats.

## Install

### Cargo (CLI)

Add the `majima` command to your command line

```bash
cargo install majima
```

### Nix (CLI)

```bash
nix-shell -p majima
```

### npm (WebAssembly)

Add majima username generation to your website

```bash
npm i majima
```
