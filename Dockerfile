FROM rust:1.68.2 as builder
WORKDIR /app
COPY . /app
RUN cargo build --release

FROM gcr.io/distroless/cc
COPY --from=builder /app/target/release/majima /
CMD ["./majima"]
