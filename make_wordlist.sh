#!/bin/sh
echo "pub static WORDS: &'static [&str] = &["
cat wordlist.txt | while read line; do
    echo "    \"$line\","
done
echo "];"
